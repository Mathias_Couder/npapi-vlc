# **VLC** Browser Plugins

This repository contains the **Webplugins** based on libVLC.
One of them is an **ActiveX** plug-in, the other is an **NPAPI** one.




### How to build 

You should be able to build it under **Linux**, **Unix**, **Mac OS** and **Mingw/MSys**.

To build this plugin from a source checkout, you should:
 - run `sh autogen.sh`
 - run `./configure`
 - run `make`

On **Windows**, you should also build it from Visual Studio


## License

Those plugins are licensed under the **GPLv2+ license**.




## CoC
The [VideoLAN Code of Conduct](https://wiki.videolan.org/CoC) applies to this project.

## Support

More information can be found [here](www.videolan.org) and [here](https://www.videolan.org/support/) is a direct link to support part

